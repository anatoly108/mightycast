﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Library;

namespace RCHttpServer
{
    using System.Web;

    internal class Program
    {
        private const string FileName = "client.rar";
        private const string PageName = "client";

        private const string ConfigPath = "Settings/config.ini";

        private static void Main(string[] args)
        {
            var iniReader = new IniFileReader(ConfigPath);
            var port = iniReader.IniReadValue("BASIC", "port");
            var isLocal = bool.Parse(iniReader.IniReadValue("BASIC", "isLocal"));
            var desiredUsername = iniReader.IniReadValue("BASIC", "username");
            var desiredPassword = iniReader.IniReadValue("BASIC", "password");

            var ip = isLocal ? "127.0.0.1" : UdpController.GetLocalIpAddress();
            var listener = new HttpListener();
            var url = string.Format("http://{0}", ip);
            var prefix = string.Format("{0}:{1}/", url, port);
            listener.Prefixes.Add(prefix);

            listener.Start();

            while (true)
            {
                var context = listener.GetContext();

                var request = context.Request;
                if (request.Url.AbsoluteUri == prefix)
                {
                    var responseText = string.Format("<html><body>"
                                                     + "<form action='{0}' method='GET'> <input name='username' type='text'/>"
                                                     + "<input name='password' type='password'/>"
                                                     + "<input type='submit'/>"
                                                     + "</form></body></html>", prefix+PageName);
                    var byteArr = System.Text.Encoding.ASCII.GetBytes(responseText);
                    context.Response.OutputStream.Write(byteArr, 0, byteArr.Length);
                    context.Response.Close();
                }
                else
                {
                    if (!request.Url.AbsoluteUri.Contains(prefix + PageName))
                    {
                        continue;
                    }

                    var resultUrl = new Uri(request.Url.AbsoluteUri);
                    var username = HttpUtility.ParseQueryString(resultUrl.Query).Get("username");
                    var password = HttpUtility.ParseQueryString(resultUrl.Query).Get("password");

                   
                    

                    if (username != desiredUsername || password != desiredPassword)
                    {
                        var responseText = string.Format("<html><body>"
                                                    + "<script type='text/javascript'>alert('Wrong data');</script>"
                                                    + "</form><a href='{0}'>Return</a></body></html>", prefix);
                        var byteArr = System.Text.Encoding.ASCII.GetBytes(responseText);
                        context.Response.OutputStream.Write(byteArr, 0, byteArr.Length);

                        continue;
                    }

                    Task.Factory.StartNew(
                            ctx => WriteFile((HttpListenerContext)ctx),
                            context,
                            TaskCreationOptions.LongRunning);
                }
            }
        }

        private static void WriteFile(HttpListenerContext ctx)
        {
            var response = ctx.Response;
            using (var fs = File.OpenRead(FileName))
            {
                response.ContentLength64 = fs.Length;
                response.SendChunked = false;
                response.ContentType = System.Net.Mime.MediaTypeNames.Application.Octet;
                response.AddHeader("Content-disposition", string.Format("attachment; filename={0}", FileName));

                var buffer = new byte[64*1024];
                using (var bw = new BinaryWriter(response.OutputStream))
                {
                    int read;
                    while ((read = fs.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        bw.Write(buffer, 0, read);
                        bw.Flush();
                    }

                    bw.Close();
                }

                response.StatusCode = (int) HttpStatusCode.OK;
                response.StatusDescription = "OK";
                response.OutputStream.Close();
            }
        }
    }
}