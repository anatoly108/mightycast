﻿namespace Mightycast
{
    public class RectangleSerializable
    {
        public int X;
        public int Y; 

        public RectangleSerializable(int x, int y)
        {
            X = x;
            Y = y;
        }

        public RectangleSerializable()
        {

        }
    }
}
