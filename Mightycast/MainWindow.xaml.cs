﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Serialization;
using Library;

namespace Mightycast
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string SettingsUdpPath = "Settings/udp.ini";
        private const string SettingsTcpPath = "Settings/tcp.ini";

        private UdpController _udpController;
        private TcpController _tcpController;
        private bool _listening = false;

        private bool _isRectDragInProg = false;

        public MainWindow()
        {
            InitializeComponent();
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            var udpIniReader = new IniFileReader(SettingsUdpPath);
            var tcpIniReader = new IniFileReader(SettingsTcpPath);

            var udpPort = int.Parse(udpIniReader.IniReadValue("BASIC", "port"));
            var multicastIp = udpIniReader.IniReadValue("BASIC", "multicastIp");

            var tcpPort = int.Parse(tcpIniReader.IniReadValue("BASIC", "port"));

            _udpController = new UdpController(udpPort, multicastIp, this);
            _tcpController = new TcpController(tcpPort, this);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            _udpController.SendMulticastQuery();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (_listening)
                return;
            Task.Factory.StartNew(()=> _udpController.Listen());
            Task.Factory.StartNew(() => _tcpController.Listen());
            _listening = true;

            ListenButton.IsEnabled = false;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            if (ConnectionsListBox.SelectedItem == null)
                return;

            var x = (int)Canvas.GetLeft(Rect);
            var y = (int)Canvas.GetTop(Rect);
            var myRectData = new RectangleSerializable(x, y);

            XmlSerializer writer = new XmlSerializer(typeof(RectangleSerializable));

            var ip = ConnectionsListBox.SelectedItem.ToString();
            _tcpController.SendRect(ip, writer, myRectData);
        }

        #region drag and drop
        private void rect_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _isRectDragInProg = true;
            Rect.CaptureMouse();
        }

        private void rect_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _isRectDragInProg = false;
            Rect.ReleaseMouseCapture();
        }

        private void rect_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_isRectDragInProg) return;

            // get the position of the mouse relative to the CanvasWithRect
            var mousePos = e.GetPosition(CanvasWithRect);

            // center the Rect on the mouse
            double left = mousePos.X - (Rect.ActualWidth / 2);
            double top = mousePos.Y - (Rect.ActualHeight / 2);
            Canvas.SetLeft(Rect, left);
            Canvas.SetTop(Rect, top);
        }
        #endregion
    }
}
