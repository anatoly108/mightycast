﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Mightycast
{
    class UdpController
    {
        private readonly int _port;
        private readonly string _multicastIp; 
        public List<string> Addresses { get; private set; }

        private readonly MainWindow _window;
        public UdpController(int port, string multicastIp, MainWindow window)
        {
            _port = port;
            _multicastIp = multicastIp;
            _window = window;
            Addresses = new List<string>();
        }

        public void Listen()
        {
            var udpClient = new UdpClient(_port);
            var groupEp = new IPEndPoint(IPAddress.Any, _port);

            try
            {
                while (true)
                {
                    Console.WriteLine("Waiting for broadcast");
                    var bytes = udpClient.Receive(ref groupEp);

                    var address = groupEp.ToString().Split(':')[0];
                    if (!Addresses.Contains(address) && address != LocalIPAddress())
                    {
                        Addresses.Add(address);
                        _window.Dispatcher.Invoke((Action)(() =>
                            {
                                _window.ConnectionsListBox.Items.Add(address);
                            }));

                        SendMulticastQuery();
                    }

                    Console.WriteLine("Received broadcast from {0} :\n {1}\n",
                        groupEp,
                        Encoding.ASCII.GetString(bytes, 0, bytes.Length));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                udpClient.Close();
            }
        }

        public void SendMulticastQuery()
        {
            var s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            var broadcast = IPAddress.Parse(_multicastIp);

            var sendbuf = Encoding.ASCII.GetBytes("Info for send");
            var ep = new IPEndPoint(broadcast, _port);

            s.SendTo(sendbuf, ep);

            Console.WriteLine("Message sent to the broadcast address");
        }

        public string LocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "";
        }
    }
}
