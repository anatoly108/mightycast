﻿using System;
using System.Net.Sockets;
using System.Xml.Serialization;
using System.Windows.Controls;

namespace Mightycast
{
    class TcpController
    {
        private readonly int _port;
        private readonly MainWindow _window;

        public TcpController(int port, MainWindow window)
        {
            _port = port;
            _window = window;
        }

        public void Listen()
        {
            var tcpListener = new TcpListener(_port);

            tcpListener.Start();
            var serializer = new XmlSerializer(typeof(RectangleSerializable));

            while (true)
            {
                var client = tcpListener.AcceptTcpClient();
                var stream = client.GetStream();
                var deserialized = (RectangleSerializable)serializer.Deserialize(stream);

                _window.Dispatcher.Invoke(() =>
                    {
                        Canvas.SetLeft(this._window.Rect, deserialized.X);
                        Canvas.SetTop(this._window.Rect, deserialized.Y);
                    });
               

                stream.Close();
                client.Close();
            }
        }

        public void SendRect(string ip, XmlSerializer serializer, RectangleSerializable rect)
        {
            var tcpClient = new TcpClient(ip, _port);
            var stream = tcpClient.GetStream();

            serializer.Serialize(stream, rect);

            tcpClient.Close();
        }

        
    }
}
