﻿using SQLite;

namespace RCServer.Model
{
    public class User
    {
        [PrimaryKey]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
    }
}