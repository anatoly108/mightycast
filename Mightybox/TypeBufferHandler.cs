﻿namespace Mightybox
{
    public static class TypeBufferHandler
    {
        public enum Type
        {
            Directory = 0, File = 1
        }

        public static byte[] CreateTypeBuffer(Type type)
        {
            var typeBuffer = new byte[FileStreamWriter.TypeBufferSize];
            typeBuffer[0] = (byte)type;
            return typeBuffer;
        }

        public static Type ReadType(byte[] bytes)
        {
            var type = (Type)bytes[0];
            return type;
        }
    }
}