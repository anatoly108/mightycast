namespace Mightybox
{
    public interface IFileSender
    {
        void Send(string filePath, string ip);
    }
}