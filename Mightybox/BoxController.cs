﻿using Library;

namespace Mightybox
{
    using System;
    using System.IO;
    using System.Net.Sockets;
    using System.Threading.Tasks;

    using NLog;

    public class BoxController
    {
        private readonly MainWindow _window;

        private readonly TcpController _tcpInfoController;

        private readonly TcpController _tcpFileController;

        private readonly TcpController _tcpStructureController;

        private FileInfoCollectionConverter FileInfoCollectionConverter { get; set; }

        private FileController FileController { get; set; }

        private string[] Files { get; set; }

        private FileInfoCollectionSerializable FileInfoCollection { get; set; }

        private readonly StructureAcceptor _structureAcceptor;

        private FileSystemWatcher _watcher;

        private readonly Logger _logger;

        private string Folder { get; set; }

        public StructureMessageSender StructureMessageSender { get; private set; }

        public delegate void DestroyMeHandler(BoxController sender);

        public event DestroyMeHandler DestroyMe;

        public BoxController(
            MainWindow window,
            TcpController tcpInfoController,
            TcpController tcpFileController,
            TcpController tcpStructureController,
            StructureAcceptor structureAcceptor,
            Logger logger,
            FileInfoCollectionConverter fileInfoCollectionConverter,
            FileController fileController,
            string folder,
            StructureMessageSender structureMessageSender)
        {
            _window = window;
            _tcpInfoController = tcpInfoController;
            _tcpFileController = tcpFileController;
            _tcpStructureController = tcpStructureController;
            _structureAcceptor = structureAcceptor;
            _logger = logger;
            FileInfoCollectionConverter = fileInfoCollectionConverter;
            FileController = fileController;
            Folder = folder;
            StructureMessageSender = structureMessageSender;

            _tcpInfoController.ConnectionEvent += TcpInfoControllerOnConnectionEvent;
            _tcpFileController.ConnectionEvent += TcpFileControllerOnConnectionEvent;
            _tcpStructureController.ConnectionEvent += TcpStructureControllerOnConnectionEvent;
            FileController.SessionStarted += FileControllerOnSessionStarted;
            FileController.SessionClosed += FileControllerOnSessionClosed;

            FileController.FileQuerySending += FileControllerOnFileQuerySending;
            FileController.FileRecieved += FileControllerOnFileRecieved;

            structureMessageSender.ConnectionError += StructureMessageSenderOnConnectionError;
        }

        private void StructureMessageSenderOnConnectionError(StructureMessageSender sender)
        {
            DestroyMe(this);
        }

        private void FileControllerOnFileRecieved(FileController sender)
        {
            _window.Dispatcher.BeginInvoke(
                (Action)(() => _window.ToRecieveNumberLabel.Content = FileController.TimesList.Count));
        }

        private void FileControllerOnFileQuerySending(FileController sender)
        {
            _window.Dispatcher.BeginInvoke(
                (Action)(() => _window.ToRecieveNumberLabel.Content = FileController.TimesList.Count));
        }

        private void TcpStructureControllerOnConnectionEvent(TcpController sender, TcpClient tcpClient)
        {
            lock (_structureAcceptor)
            {
                var ipFromTcpClient = _tcpStructureController.GetIpFromTcpClient(tcpClient);
                try
                {
                    _structureAcceptor.Accept(ipFromTcpClient, tcpClient.GetStream(), FileInfoCollection);
                }
                catch (Exception exception)
                {
                    DestroyMe(this);
                }
            }
            UpdateFilesList();
        }

        private void FileControllerOnSessionClosed(FileController sender)
        {
            StartWatch();
            _window.Dispatcher.BeginInvoke((Action)(() => _window.IsSessionStartedLabel.Content = "Нет"));
            StructureMessageSender.SendOrdinaryStructureMessage(WatcherChangeTypes.Changed, FileInfoCollection);
        }

        private void FileControllerOnSessionStarted(FileController sender)
        {
            StopWatch();
            _window.Dispatcher.BeginInvoke((Action)(() => _window.IsSessionStartedLabel.Content = "Да"));
        }

        private void TcpInfoControllerOnConnectionEvent(TcpController sender, TcpClient tcpClient)
        {
            // Тут принимаем запрос на файл
            try
            {
                var fileName = NetworkHelper.GetStringFromNetworkStream(tcpClient.GetStream());

                var ip = _tcpInfoController.GetIpFromTcpClient(tcpClient);
                FileController.Send(fileName, ip);
            }
            catch (Exception exception)
            {
                DestroyMe(this);
            }
        }

        private void TcpFileControllerOnConnectionEvent(TcpController sender, TcpClient tcpClient)
        {
            try
            {
                FileController.Accept(tcpClient.GetStream());
            }
            catch (Exception exception)
            {
                DestroyMe(this);
                return;
            }
            UpdateFilesList();
        }

        private void UpdateFilesList()
        {
            try
            {
                var files = Directory.GetFileSystemEntries(Folder, "*.*", SearchOption.AllDirectories);
                foreach (var file in files)
                {
                    var file1 = file;
                    var attr = File.GetAttributes(file1);
                    if (attr == FileAttributes.Directory)
                    {
                        var content = Directory.GetFileSystemEntries(file1, "*.*", SearchOption.AllDirectories);
                        if (content.Length > 0)
                        {
                            continue;
                        }
                    }
                }
                _logger.Info("Files list updated");
                FileInfoCollection = FileInfoCollectionConverter.Convert(files);
                Files = files;
                _window.Dispatcher.BeginInvoke(
                    (Action)(() => _window.FileCountLabel.Content = FileInfoCollection.FileInfoSerializables.Count));
            }
            catch (Exception exception)
            {
                _logger.Error(exception.ToString());
            }
        }

        public void Start()
        {
            _watcher = new FileSystemWatcher(Folder);

            const NotifyFilters Filter =
                NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName
                | NotifyFilters.DirectoryName;
            _watcher.NotifyFilter = Filter;

            _watcher.IncludeSubdirectories = true;
            _watcher.EnableRaisingEvents = true;

            StartWatch();

            _window.Dispatcher.BeginInvoke((Action)(() => _window.ToRecieveNumberLabel.Content = "0"));
            _window.Dispatcher.BeginInvoke((Action)(() => _window.IsSessionStartedLabel.Content = "Нет"));
            UpdateFilesList();

            StructureMessageSender.Start();

            Task.Factory.StartNew(() => _tcpInfoController.Listen());
            Task.Factory.StartNew(() => _tcpFileController.Listen());
            Task.Factory.StartNew(() => _tcpStructureController.Listen());

            StructureMessageSender.SendFirstTimeStructureMessage(FileInfoCollection);
        }

        public void Stop()
        {
            FileController.Reboot();
            _tcpFileController.StopListen();
            _tcpInfoController.StopListen();
            _tcpStructureController.StopListen();
            StructureMessageSender.Stop();
        }

        private void StartWatch()
        {
            StopWatch();

            _watcher.Changed += WatcherOnChanged;
            _watcher.Created += WatcherOnChanged;
            _watcher.Deleted += WatcherOnChanged;
            _watcher.Renamed += WatcherOnChanged;
            _logger.Info("Watching started");
        }

        private void StopWatch()
        {
            _watcher.Changed -= WatcherOnChanged;
            _watcher.Created -= WatcherOnChanged;
            _watcher.Deleted -= WatcherOnChanged;
            _watcher.Renamed -= WatcherOnChanged;
            _logger.Info("Watching stopped");
        }

        private void WatcherOnChanged(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            UpdateFilesList();
            var fullPath = fileSystemEventArgs.FullPath;
            if (fullPath.ToLower().Contains(".tmp"))
            {
                return;
            }

            StructureMessageSender.SendOrdinaryStructureMessage(fileSystemEventArgs.ChangeType, FileInfoCollection);
        }
    }
}