﻿namespace Mightybox
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using NLog;

    public class StructureAnalyzer
    {
        private readonly Logger _logger;

        public StructureAnalyzer(Logger logger)
        {
            _logger = logger;
        }

        public IEnumerable<string> Analyze(FileInfoCollectionSerializable ourCollection, FileInfoCollectionSerializable theirCollection)
        {
            var filesToRecieve = new List<string>();

            foreach (var fileInfo in theirCollection.FileInfoSerializables)
            {
                var sameNameFile = ourCollection.FileInfoSerializables.FirstOrDefault(x => x.Name == fileInfo.Name);
                if (sameNameFile != null)
                {
                    if (sameNameFile.LastWriteTime < fileInfo.LastWriteTime)
                    {
                        if(!Directory.Exists(sameNameFile.Name) || Directory.GetFileSystemEntries(sameNameFile.Name, "*.*", SearchOption.AllDirectories).Length == 0)
                            filesToRecieve.Add(fileInfo.Name);
                    }
                }
                else
                {
                    filesToRecieve.Add(fileInfo.Name);
                }
            }

            _logger.Info("Files to recieve:");
            foreach (var file in filesToRecieve)
            {
                _logger.Info(file);
            }
            _logger.Info("---");

            return filesToRecieve;
        } 
    }
}