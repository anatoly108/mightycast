﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Mightybox
{
    using System.Net.Sockets;

    using Library;

    using NLog;

    public class FileController
    {
        private readonly IFileAcceptor _fileAcceptor;

        private readonly IFileSender _fileSender;

        private readonly IFileQuerySender _fileQuerySender;

        public delegate void SessionEventHandler(FileController sender);

        public delegate void FileQuerySendingHandler(FileController sender);

        public delegate void FileRecievedHandler(FileController sender);

        public event SessionEventHandler SessionStarted;

        public event SessionEventHandler SessionClosed;

        public event FileQuerySendingHandler FileQuerySending;

        public event FileRecievedHandler FileRecieved;

        public readonly Dictionary<string, DateTime> TimesList;

        private bool IsSessionActivated { get; set; }

        private int _expectingFilesNumber;

        private readonly Logger _logger;

        public FileController(
            IFileAcceptor fileAcceptor,
            IFileSender fileSender,
            IFileQuerySender fileQuerySender,
            Logger logger)
        {
            _fileAcceptor = fileAcceptor;
            _fileSender = fileSender;
            _fileQuerySender = fileQuerySender;
            _logger = logger;
            IsSessionActivated = false;
            TimesList = new Dictionary<string, DateTime>();
        }

        public void Send(string filePath, string ip)
        {
            _fileSender.Send(filePath, ip);
        }

        public void SendQuery(string ip, string fileName, DateTime timeForFile)
        {
            if (TimesList.ContainsKey(fileName))
            {
                return;
            }

            if (!IsSessionActivated)
            {
                IsSessionActivated = true;
                if (SessionStarted != null)
                {
                    SessionStarted(this);
                }
            }
            FileQuerySending(this);
            _fileQuerySender.Send(ip, fileName);
            _expectingFilesNumber++;
            TimesList.Add(fileName, timeForFile);
        }

        public void Accept(NetworkStream stream)
        {
            try
            {
                var fileName = _fileAcceptor.GetFileNameFromStream(stream);
                if (!TimesList.ContainsKey(fileName))
                {
                    return;
                }
                try
                {
                    if (File.Exists(fileName))
                    {
                        File.SetAttributes(fileName, FileAttributes.Normal);
                    }
                    _fileAcceptor.Accept(stream, fileName);
                }
                catch (Exception ex)
                {
                    _logger.Error(ex.ToString());
                    return;
                }

                DateTime time;
                fileName = fileName.Replace(@"\\", @"\");
                var attr = File.GetAttributes(fileName);
                TimesList.TryGetValue(fileName, out time);
                if (attr != FileAttributes.Directory)
                {
                    File.SetLastWriteTime(fileName, time);
                }
                FileRecieved(this);
                TimesList.Remove(fileName);
                _expectingFilesNumber--;

                if (_expectingFilesNumber == 0)
                {
                    CloseSession();
                }
            }
            catch (Exception exception)
            {
                _logger.Error(exception.ToString());
            }
        }

        private void CloseSession()
        {
            IsSessionActivated = false;
            SessionClosed(this);
        }

        public void Reboot()
        {
            TimesList.Clear();
            CloseSession();
        }
    }
}