﻿using System.Linq;

using Mightybox.Processors;

namespace Mightybox
{
    using System;
    using System.IO;
    using System.Net.Sockets;
    using System.Xml.Serialization;

    using Library;

    using NLog;

    public class StructureAcceptor
    {

        private readonly RenameProcessor _renameProcessor;

        private readonly DeleteProcessor _deleteProcessor;

        private readonly NewFolderProcessor _newFolderProcessor;

        private readonly Logger _logger;

        private readonly XmlSerializer _collectionSerializer;

        private readonly StructureAnalyzer _structureAnalyzer;

        private readonly FileController _fileController;

        private readonly StructureMessageSender _structureMessageSender;

        public StructureAcceptor(Logger logger, XmlSerializer collectionSerializer, StructureAnalyzer structureAnalyzer, FileController fileController, StructureMessageSender structureMessageSender)
        {
            _logger = logger;
            _collectionSerializer = collectionSerializer;
            _structureAnalyzer = structureAnalyzer;
            _fileController = fileController;
            _structureMessageSender = structureMessageSender;

            _renameProcessor = new RenameProcessor();
            _deleteProcessor = new DeleteProcessor();
            _newFolderProcessor = new NewFolderProcessor();
        }

        public void Accept(string ip, NetworkStream stream, FileInfoCollectionSerializable fileInfoCollection)
        {
            var wholeBuffer = NetworkHelper.ReadFromNetworkStream(stream);
            var typeIndex = wholeBuffer.Length - 1;

            var byteForType = wholeBuffer[typeIndex];
            var type = (StructureSender.QueryType)byteForType;
            var deserBuffer = new byte[wholeBuffer.Length - 1];
            Array.Copy(wholeBuffer, deserBuffer, wholeBuffer.Length - 1);

            var deserializeStream = new MemoryStream(deserBuffer);
            var theirCollection =
                (FileInfoCollectionSerializable)_collectionSerializer.Deserialize(deserializeStream);
            var ourCollection = fileInfoCollection;

            _logger.Info(string.Format("Got structure from {0}, type: {1}", ip, type));
            switch (type)
            {
                case StructureSender.QueryType.InfoFirstTime:
                case StructureSender.QueryType.Info:
                    var needed = _structureAnalyzer.Analyze(ourCollection, theirCollection);

                    foreach (var file in needed)
                    {
                        // Отправляем запрос на получение файла
                        var time = theirCollection.FileInfoSerializables.First(x => x.Name == file).LastWriteTime;
                        _fileController.SendQuery(ip, file, time);
                    }
                    if (type == StructureSender.QueryType.InfoFirstTime)
                    {
                        _structureMessageSender.SendOrdinaryStructureMessage(WatcherChangeTypes.Changed, fileInfoCollection);
                    }
                    break;
                case StructureSender.QueryType.Renamed:
                    _renameProcessor.Handle(ourCollection, theirCollection);
                    break;
                case StructureSender.QueryType.Deleted:
                    _deleteProcessor.Handle(ourCollection, theirCollection);
                    break;
                case StructureSender.QueryType.NewFolder:
                    _newFolderProcessor.Handle(ourCollection, theirCollection);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}