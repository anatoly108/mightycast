﻿using System.Xml.Serialization;

namespace Mightybox
{
    using System;

    public class FileInfoSerializable
    {
        public string Name { get; set; }
        public DateTime LastWriteTime { get; set; }

        public FileInfoSerializable()
        {
        }

        public FileInfoSerializable(string name, DateTime lastWriteTime)
        {
            Name = name;
            LastWriteTime = lastWriteTime;
        }
    }
}