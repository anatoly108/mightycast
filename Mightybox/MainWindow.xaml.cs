﻿using System.Windows;

namespace Mightybox
{
    using System;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BoxController _boxController;

        private readonly BoxControllerFactory _factory;

        public MainWindow()
        {
            InitializeComponent();
            _factory = new BoxControllerFactory(Constants.SettingPath);
        }

        private void BoxControllerOnDestroyMe(BoxController sender)
        {
            StopBoxController();
            _factory.Logger.Info("Destroyed. Sorry");
            MessageBox.Show("Ошибка соединения. Нажмите 'Старт' на форме приложения");
        }

        private void StartButtonClick(object sender, RoutedEventArgs e)
        {
            StartBoxController();
        }

        private void StopButton_OnClick(object sender, RoutedEventArgs e)
        {
            StopBoxController();
            ToRecieveNumberLabel.Content = "-";
            IsSessionStartedLabel.Content = "-";
            FileCountLabel.Content = "-";
        }

        private void StopBoxController()
        {
            _boxController.Stop();
            _boxController = null;
            Dispatcher.BeginInvoke((Action)(() =>
                {
                    StartButton.IsEnabled = true;
                    StopButton.IsEnabled = false;
                }));
            
        }

        private void StartBoxController()
        {
            _boxController = _factory.Create(this);
            _boxController.DestroyMe += BoxControllerOnDestroyMe;
            _boxController.Start();
            Dispatcher.BeginInvoke((Action)(() =>
            {
                StartButton.IsEnabled = false;
                StopButton.IsEnabled = true;
            }));
        }
    }
}
