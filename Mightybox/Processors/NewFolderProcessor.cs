﻿using System.IO;
using System.Linq;

namespace Mightybox.Processors
{
    public class NewFolderProcessor
    {
        public void Handle(FileInfoCollectionSerializable our, FileInfoCollectionSerializable their)
        {
            foreach (var theirFile in their.FileInfoSerializables)
            {
                var ourFolder = our.FileInfoSerializables.FirstOrDefault(x => x.Name == theirFile.Name);

                if (ourFolder == null)
                {
                    Directory.CreateDirectory(theirFile.Name);
                }
            }
        } 
    }
}