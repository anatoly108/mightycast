﻿using System.IO;
using System.Linq;

namespace Mightybox.Processors
{
    public class DeleteProcessor
    {
        public void Handle(FileInfoCollectionSerializable our, FileInfoCollectionSerializable their)
        {
            foreach (var fileInfo in our.FileInfoSerializables)
            {
                var theirFile = their.FileInfoSerializables.FirstOrDefault(x => x.Name == fileInfo.Name);

                if (theirFile == null)
                {
                    if (Directory.Exists(fileInfo.Name))
                    {
                        Directory.Delete(fileInfo.Name, true);
                    }
                    else
                    {
                        File.Delete(fileInfo.Name);
                    }
                    return;
                }
            }
        }
    }
}