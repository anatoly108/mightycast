﻿using System.IO;
using System.Linq;

namespace Mightybox.Processors
{
    public class RenameProcessor
    {
        public void Handle(FileInfoCollectionSerializable our, FileInfoCollectionSerializable their)
        {
            var renamedFile = "";

            foreach (var fileInfo in our.FileInfoSerializables)
            {
                var theirFile = their.FileInfoSerializables.FirstOrDefault(x => x.Name == fileInfo.Name);

                if (theirFile == null)
                {
                    renamedFile = fileInfo.Name;
                    break;
                }
            }

            foreach (var theirFile in their.FileInfoSerializables)
            {
                var ourFile = our.FileInfoSerializables.FirstOrDefault(x => x.Name == theirFile.Name);

                if (ourFile == null)
                {
                    var attr = File.GetAttributes(renamedFile);
                    if (attr == FileAttributes.Directory)
                    {
                        Directory.Move(renamedFile, theirFile.Name);
                    }
                    else
                    {
                        File.Move(renamedFile, theirFile.Name);    
                    }
                    break;
                }
            }
        }
    }
}