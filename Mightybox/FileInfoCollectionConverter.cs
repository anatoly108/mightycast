﻿namespace Mightybox
{
    using System.Collections.Generic;
    using System.IO;

    public class FileInfoCollectionConverter
    {
        public FileInfoCollectionSerializable Convert(IEnumerable<string> files)
        {
            var filesInfos = new List<FileInfoSerializable>();
            foreach (var file in files)
            {
                var fileInfo = new FileInfo(file);
                var info = new FileInfoSerializable(file, fileInfo.LastWriteTime);
                filesInfos.Add(info);
            }

            var collection = new FileInfoCollectionSerializable(filesInfos);
            return collection;
        } 
    }
}