﻿using System.Security.AccessControl;

namespace Mightybox
{
    using System.IO;
    using System.Xml.Serialization;

    using Library;

    using NLog;

    public class BoxControllerFactory
    {
        private readonly string _folder;

        private readonly int _udpPort;

        private readonly string _multicastIp;

        private readonly int _tcpInfoPort;

        private readonly int _tcpFilePort;

        private readonly int _tcpStructurePort;

        private readonly int _tcpStructureQueryPort;

        public Logger Logger;

        public BoxController Create(MainWindow window)
        {
            Logger = LogManager.GetLogger("Log");

            Directory.CreateDirectory(_folder);
            File.SetAttributes(_folder, FileAttributes.Normal);
            DirectoryInfo di = new DirectoryInfo(_folder);
            di.Attributes = FileAttributes.Normal;
            window.SyncFolderTextBox.Text = _folder;
            
            var collectionSerializer = new XmlSerializer(typeof(FileInfoCollectionSerializable));
            
            var udpController = new UdpController(_udpPort, _multicastIp, Logger);
            
            var tcpInfoController = new TcpController(_tcpInfoPort, Logger);
            var tcpFileController = new TcpController(_tcpFilePort, Logger);
            var tcpStructureController = new TcpController(_tcpStructurePort, Logger);
            var tcpQueryForStructureController = new TcpController(_tcpStructureQueryPort, Logger);

            var fileInfoCollectionConverter = new FileInfoCollectionConverter();
            var structureAnalyzer = new StructureAnalyzer(Logger);

            var fileQuerySender = new FileQuerySender(tcpInfoController, Logger);
            var fileSender = new FileSender(_tcpFilePort, Logger);
            var fileAcceptor = new FileAcceptor(Logger);

            var fileController = new FileController(fileAcceptor, fileSender, fileQuerySender, Logger);

            var structureSender = new StructureSender(Logger, tcpStructureController);

            var structureMessageSender = new StructureMessageSender(udpController, tcpQueryForStructureController, Logger, structureSender, collectionSerializer);
            var structureAcceptor = new StructureAcceptor(Logger, collectionSerializer, structureAnalyzer, fileController, structureMessageSender);
            
            var boxController = new BoxController(
                window,
                tcpInfoController,
                tcpFileController,
                tcpStructureController,
                structureAcceptor,
                Logger,
                fileInfoCollectionConverter,
                fileController,
                _folder, structureMessageSender);

            return boxController;
        }

        public BoxControllerFactory(string settingsPath)
        {
            var iniReader = new IniFileReader(settingsPath);
            _folder = iniReader.IniReadValue("BASIC", "folder");
            _udpPort = int.Parse(iniReader.IniReadValue("BASIC", "udpPort"));
            _multicastIp = iniReader.IniReadValue("BASIC", "multicastIp");
            _tcpInfoPort = int.Parse(iniReader.IniReadValue("BASIC", "tcpInfoPort"));
            _tcpFilePort = int.Parse(iniReader.IniReadValue("BASIC", "tcpFilePort"));
            _tcpStructurePort = int.Parse(iniReader.IniReadValue("BASIC", "tcpStructurePort"));
            _tcpStructureQueryPort = int.Parse(iniReader.IniReadValue("BASIC", "tcpStructureQueryPort"));
        }
    }
}