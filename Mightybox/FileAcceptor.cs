﻿namespace Mightybox
{
    using System;
    using System.IO;

    using NLog;

    //! Класс реализует функционал получения файла из потока
    public class FileAcceptor : IFileAcceptor
    {
        private readonly Logger _logger; ///< Логгер
        private const int BufferSize = 1024; ///< Размер буфера передачи файла (кб)

        //! Конструктор принимает логгер
        public FileAcceptor(Logger logger)
        {
            _logger = logger;
        }

        //! Функция принятия файла
        /*!
         * @param stream поток
         * @param fileName имя, с которым будет сохранён файл
         */
        public void Accept(Stream stream, string fileName)
        {
            var recData = new byte[BufferSize];

            var typeBytes = new byte[FileStreamWriter.TypeBufferSize];
            stream.Read(typeBytes, 0, FileStreamWriter.TypeBufferSize);
            var type = TypeBufferHandler.ReadType(typeBytes);

            switch (type)
            {
                case TypeBufferHandler.Type.Directory:
                    Directory.CreateDirectory(fileName);
                    _logger.Info(string.Format("Got directory {0}", fileName));
                    break;
                case TypeBufferHandler.Type.File:
                    var parentName = Directory.GetParent(fileName).FullName;
                    Directory.CreateDirectory(parentName);

                    var memoryStream = new MemoryStream();
                    int recBytes;
                    while ((recBytes = stream.Read(recData, 0, recData.Length)) > 0)
                    {
                        memoryStream.Write(recData, 0, recBytes);
                    }

                    memoryStream.Seek(0, SeekOrigin.Begin);

                    using (var fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        memoryStream.WriteTo(fs);
                    }
                    _logger.Info(string.Format("File {0} saved", fileName));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        //! Получение имени файла из потока
        /*!
         * @param stream поток
         */
        public string GetFileNameFromStream(Stream stream)
        {
            var nameBytes = new byte[FileStreamWriter.FileNameBufferSize];
            stream.Read(nameBytes, 0, FileStreamWriter.FileNameBufferSize);
            var fileName = Library.Constants.Encoding.GetString(nameBytes).TrimEnd();
            return fileName;
        }
    }
}