﻿namespace Mightybox
{
    using System.Collections.Generic;

    public class FileInfoCollectionSerializable
    {
        public List<FileInfoSerializable> FileInfoSerializables { get; set; }

        public FileInfoCollectionSerializable(List<FileInfoSerializable> fileInfoSerializables)
        {
            FileInfoSerializables = fileInfoSerializables;
        }

        public FileInfoCollectionSerializable()
        {
        }
    }
}