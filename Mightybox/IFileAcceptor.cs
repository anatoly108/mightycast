namespace Mightybox
{
    using System.IO;

    public interface IFileAcceptor
    {
        void Accept(Stream stream, string fileName);
        string GetFileNameFromStream(Stream stream);
    }
}