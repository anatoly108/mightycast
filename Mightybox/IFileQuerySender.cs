﻿namespace Mightybox
{
    public interface IFileQuerySender
    {
        void Send(string ip, string fileName);
    }
}