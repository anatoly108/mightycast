﻿using Library;

namespace Mightybox
{
    using System;
    using System.IO;
    using System.Xml.Serialization;

    using NLog;

    public class StructureSender
    {
        private readonly Logger _logger;

        private const int TypeBufferSize = 1;

        private readonly TcpController _tcpController;

        public StructureSender(Logger logger, TcpController tcpController)
        {
            _logger = logger;
            _tcpController = tcpController;
        }

        public enum QueryType
        {
            Info = 0, Renamed = 1, Deleted = 2, InfoFirstTime = 3, NewFolder = 4
        }

        public void SendStructure(FileInfoCollectionSerializable collection, XmlSerializer xmlSerializer, QueryType type, string ip)
        {
            var stream = new MemoryStream();
            xmlSerializer.Serialize(stream, collection);

            byte[] buf;

            switch (type)
            {
                case QueryType.Info:
                    buf = new byte[] { 0 };
                stream.Write(buf, 0, TypeBufferSize);
                    break;
                case QueryType.Renamed:
                    buf = new byte[] { 1 };
                stream.Write(buf, 0, TypeBufferSize);
                    break;
                case QueryType.Deleted:
                    buf = new byte[] { 2 };
                stream.Write(buf, 0, TypeBufferSize);
                    break;
                case QueryType.InfoFirstTime:
                    buf = new byte[] { 3 };
                    stream.Write(buf, 0, TypeBufferSize);
                    break;
                case QueryType.NewFolder:
                    buf = new byte[] { 4 };
                    stream.Write(buf, 0, TypeBufferSize);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("type");
            }

            stream.Seek(0, SeekOrigin.Begin);

            var bytes = new byte[stream.Length];
            stream.Read(bytes, 0, (int)stream.Length);

             _tcpController.SendData(bytes, ip);

            _logger.Info(("Structure sent, query type: " + type));
        } 
    }
}