﻿using Library;

namespace Mightybox
{
    using NLog;

    public class FileQuerySender : IFileQuerySender
    {
        private readonly TcpController _tcpController;

        private readonly Logger _logger;

        public FileQuerySender(TcpController tcpController, Logger logger)
        {
            _tcpController = tcpController;
            _logger = logger;
        }

        public void Send(string ip, string fileName)
        {
            _tcpController.SendText(fileName, ip);
            _logger.Info("Query for file {0} sent to the address {1}", fileName, ip);
        }
    }
}