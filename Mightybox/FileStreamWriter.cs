﻿namespace Mightybox
{
    using System;
    using System.IO;

    using Library;

    public class FileStreamWriter
    {
        public const int FileNameBufferSize = 256;

        public const int TypeBufferSize = 1;

        private const int BufferSize = 1024;

        public void Write(Stream stream, string filePath, string pathToEncode = "")
        {
            if (Directory.Exists(filePath))
            {
                WriteDirectory(stream, filePath, pathToEncode);
            }
            else
            {
                WriteFile(stream, filePath, pathToEncode);    
            }

        }

        private void WriteFile(Stream stream, string filePath, string pathToEncode)
        {
            using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                var noOfPackets = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(fs.Length) / Convert.ToDouble(BufferSize)));
                var totalLength = (int)fs.Length;

                string fileName;
                byte[] fileNameBuffer;
                if (pathToEncode == "")
                {
                    fileName = HandleFileName(filePath);
                    fileNameBuffer = Library.Constants.Encoding.GetBytes(fileName);
                }
                else
                {
                    fileName = HandleFileName(pathToEncode);
                    fileNameBuffer = Library.Constants.Encoding.GetBytes(fileName);
                }

                stream.Write(fileNameBuffer, 0, FileNameBufferSize);

                var typeBuffer = TypeBufferHandler.CreateTypeBuffer(TypeBufferHandler.Type.File);
                stream.Write(typeBuffer, 0, TypeBufferSize);

                for (var i = 0; i < noOfPackets; i++)
                {
                    int currentPacketLength;
                    if (totalLength > BufferSize)
                    {
                        currentPacketLength = BufferSize;
                        totalLength = totalLength - currentPacketLength;
                    }
                    else
                    {
                        currentPacketLength = totalLength;
                    }
                    var sendingBuffer = new byte[currentPacketLength];
                    fs.Read(sendingBuffer, 0, currentPacketLength);
                    stream.Write(sendingBuffer, 0, sendingBuffer.Length);
                }
            }
        }

        private void WriteDirectory(Stream stream, string filePath, string pathToEncode)
        {
            string fileName;
            byte[] fileNameBuffer;
            if (pathToEncode == "")
            {
                fileName = HandleFileName(filePath);
                fileNameBuffer = Library.Constants.Encoding.GetBytes(fileName);
            }
            else
            {
                fileName = HandleFileName(pathToEncode);
                fileNameBuffer = Library.Constants.Encoding.GetBytes(fileName);
            }

            stream.Write(fileNameBuffer, 0, FileNameBufferSize);

            var typeBuffer = TypeBufferHandler.CreateTypeBuffer(TypeBufferHandler.Type.Directory);
            stream.Write(typeBuffer, 0, TypeBufferSize);
        }

        private string HandleFileName(string filePath)
        {
            var fileName = filePath;
            if (fileName.Length > FileNameBufferSize)
            {
                throw new Exception("Слишком длинный путь к файлу. Текущий размер буфера имени - " + FileNameBufferSize);
            }

            if (fileName.Length < FileNameBufferSize)
            {
                var needed = FileNameBufferSize - fileName.Length;
                for (var i = 0; i < needed; i++)
                {
                    fileName += " ";
                }
            }

            return fileName;
        }
    }
}