﻿namespace Mightybox
{
    using System;
    using System.Net.Sockets;

    using NLog;

    public class FileSender : IFileSender
    {
        private readonly int _port;
        private readonly FileStreamWriter _fileStreamWriter;

        private readonly Logger _logger;

        public FileSender(int port, Logger logger)
        {
            _port = port;
            _logger = logger;
            _fileStreamWriter = new FileStreamWriter();
        }

        public void Send(string filePath, string ip)
        {
            TcpClient client = null;
            NetworkStream netstream = null;
            try
            {
                client = new TcpClient(ip, _port);
                netstream = client.GetStream();

                _fileStreamWriter.Write(netstream, filePath);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
            finally
            {
                if(netstream != null)
                    netstream.Close();
                if(client != null)
                    client.Close();
            }
        }
    }
}