﻿namespace Mightybox
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Net.Sockets;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Serialization;

    using Library;

    using NLog;

    public class StructureMessageSender
    {
        public UdpController UdpController { get; private set; }

        private readonly TcpController _tcpQueryForStructureController;

        private readonly Logger _logger;

        private readonly Dictionary<int, WatcherChange> _changesEvents = new Dictionary<int, WatcherChange>();

        private int _sendIndex = 0;

        private readonly object _indexLock = new object();

        private readonly StructureSender _structureSender;

        private readonly XmlSerializer _collectionSerializer;

        public delegate void ConnectionErrorHandler(StructureMessageSender sender);

        public event ConnectionErrorHandler ConnectionError;

        private struct WatcherChange
        {
            public string FullPath { get; private set; }

            public WatcherChangeTypes WatcherChangeType { get; private set; }

            public FileInfoCollectionSerializable FileInfoCollection { get; private set; }

            public bool InfoFirstTime { get; private set; }

            public WatcherChange(
                string fullPath,
                WatcherChangeTypes watcherChangeType,
                FileInfoCollectionSerializable fileInfoCollection,
                bool infoFirstTime)
                : this()
            {
                FullPath = fullPath;
                WatcherChangeType = watcherChangeType;
                FileInfoCollection = fileInfoCollection;
                InfoFirstTime = infoFirstTime;
            }
        }

        public StructureMessageSender(
            UdpController udpController,
            TcpController tcpQueryForStructureController,
            Logger logger,
            StructureSender structureSender,
            XmlSerializer collectionSerializer)
        {
            UdpController = udpController;
            _tcpQueryForStructureController = tcpQueryForStructureController;
            _logger = logger;
            _structureSender = structureSender;
            _collectionSerializer = collectionSerializer;

            UdpController.ConnectionEvent += UdpControllerOnConnectionEvent;
            _tcpQueryForStructureController.ConnectionEvent += TcpQueryForStructureControllerOnConnectionEvent;
        }

        private void TcpQueryForStructureControllerOnConnectionEvent(TcpController sender, TcpClient tcpClient)
        {
            try
            {
                var reader = new StreamReader(tcpClient.GetStream(), Library.Constants.Encoding);
                var index = int.Parse(reader.ReadToEnd());

                var ip = _tcpQueryForStructureController.GetIpFromTcpClient(tcpClient);

                _logger.Info(string.Format("Machine {0} wants structure with index {1}", ip, index));

                WatcherChange watcherChange;
                var isGot = _changesEvents.TryGetValue(index, out watcherChange);
                if (!isGot)
                {
                    _logger.Error("Нет изменения " + index);
                    return;
                }

                var fileInfoCollection = watcherChange.FileInfoCollection;

                if (watcherChange.InfoFirstTime)
                {
                    _structureSender.SendStructure(
                        fileInfoCollection,
                        _collectionSerializer,
                        StructureSender.QueryType.InfoFirstTime,
                        ip);
                    return;
                }

                switch (watcherChange.WatcherChangeType)
                {
                    case WatcherChangeTypes.Created:
                        if (Directory.Exists(watcherChange.FullPath))
                        {
                            _structureSender.SendStructure(
                                fileInfoCollection,
                                _collectionSerializer,
                                StructureSender.QueryType.NewFolder,
                                ip);
                            break;
                        }
                        _structureSender.SendStructure(
                            fileInfoCollection,
                            _collectionSerializer,
                            StructureSender.QueryType.Info,
                            ip);
                        break;
                    case WatcherChangeTypes.Deleted:
                        if (!File.Exists(watcherChange.FullPath))
                        {
                            _structureSender.SendStructure(
                                fileInfoCollection,
                                _collectionSerializer,
                                StructureSender.QueryType.Deleted,
                                ip);
                        }
                        break;
                    case WatcherChangeTypes.Changed:
                        _structureSender.SendStructure(
                            fileInfoCollection,
                            _collectionSerializer,
                            StructureSender.QueryType.Info,
                            ip);
                        break;
                    case WatcherChangeTypes.Renamed:
                        _structureSender.SendStructure(
                            fileInfoCollection,
                            _collectionSerializer,
                            StructureSender.QueryType.Renamed,
                            ip);
                        break;
                    case WatcherChangeTypes.All:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception exception)
            {
                ConnectionError(this);
            }
        }

        private void UdpControllerOnConnectionEvent(UdpController sender, string ip, MemoryStream stream)
        {
            var reader = new StreamReader(stream, Library.Constants.Encoding);
            var index = reader.ReadToEnd();

            _logger.Info(string.Format("Got structure message from {0} with index {1}", ip, index));

            _tcpQueryForStructureController.SendText(index, ip);
        }

        public void SendOrdinaryStructureMessage(
            WatcherChangeTypes watcherChangeTypes,
            FileInfoCollectionSerializable fileInfoCollection)
        {
            lock (_indexLock)
            {
                var value = new WatcherChange(null, watcherChangeTypes, fileInfoCollection, false);
                _changesEvents.Add(_sendIndex, value);
                Thread.Sleep(500);
                UdpController.SendMulticast(_sendIndex.ToString());
                _sendIndex++;
            }
            _logger.Info("Multicast structure message sent");
        }

        public void SendFirstTimeStructureMessage(FileInfoCollectionSerializable fileInfoCollection)
        {
            lock (_indexLock)
            {
                const WatcherChangeTypes WatcherChangeTypes = WatcherChangeTypes.Changed;
                var value = new WatcherChange(null, WatcherChangeTypes, fileInfoCollection, true);
                _changesEvents.Add(_sendIndex, value);
                Thread.Sleep(500);
                UdpController.SendMulticast(_sendIndex.ToString());
                _sendIndex++;
            }
            _logger.Info("Multicast first time structure message sent");
        }

        public void Start()
        {
            Task.Factory.StartNew(() => UdpController.Listen());
            Task.Factory.StartNew(() => _tcpQueryForStructureController.Listen());
        }

        public void Stop()
        {
            _tcpQueryForStructureController.StopListen();
            UdpController.StopListen();
        }
    }
}