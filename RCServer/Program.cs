﻿using System;

using Library;

using RCServer.Commands;

namespace RCServer
{
    internal class Program
    {
        private const string ConfigPath = "Settings\\config.ini";

        private static void Main(string[] args)
        {
            var iniReader = new IniFileReader(ConfigPath);
            var iniReadValue = iniReader.IniReadValue("BASIC", "serverPort");
            var serverPort = int.Parse(iniReadValue);
            var clientPort = int.Parse(iniReader.IniReadValue("BASIC", "clientPort"));
            var authPort = int.Parse(iniReader.IniReadValue("BASIC", "authPort"));
            var isLocal = bool.Parse(iniReader.IniReadValue("BASIC", "isLocal"));

            var ip = UdpController.GetLocalIpAddress();
            if (isLocal)
            {
                ip = "127.0.0.1";
            }

            var authTcpListener = new AuthenticatingTcpListener(ip, authPort);

            var recieverTcpController = new TcpController(clientPort, null);
            var senderTcpController = new TcpController(serverPort, null);
            var commandValidator = new CommandValidator();
            var commandExecutor = new CommandExecutor(senderTcpController, recieverTcpController);
            var controller = new ServerController(
                senderTcpController,
                commandValidator,
                commandExecutor,
                recieverTcpController,
                authTcpListener);

            controller.Start();

            Console.ReadKey();
        }

        /**
         * Аутентификация:
         * 
using(PrincipalContext pc = new PrincipalContext(ContextType.Domain, "YOURDOMAIN"))
{
    bool isValid = pc.ValidateCredentials("myuser", "mypassword");
}
         */
    }
}
