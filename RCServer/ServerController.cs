﻿using System;
using System.Net.Sockets;
using System.Threading.Tasks;
using Library;
using RCServer.Commands;

namespace RCServer
{
    public class ServerController
    {
        private readonly TcpController _senderTcpController;
        private readonly TcpController _recieverTcpController;

        private readonly AuthenticatingTcpListener _authTcpListener;

        private readonly CommandValidator _commandValidator;
        private readonly CommandExecutor _commandExecutor;

        public ServerController(TcpController senderTcpController, CommandValidator commandValidator, CommandExecutor commandExecutor, TcpController recieverTcpController, AuthenticatingTcpListener authTcpListener)
        {
            _senderTcpController = senderTcpController;
            _commandValidator = commandValidator;
            _commandExecutor = commandExecutor;
            _recieverTcpController = recieverTcpController;
            _authTcpListener = authTcpListener;
            _recieverTcpController.ConnectionEvent += RecieverTcpControllerOnConnectionEvent;
        }

        private void RecieverTcpControllerOnConnectionEvent(TcpController sender, TcpClient tcpClient)
        {
            var ip = _recieverTcpController.GetIpFromTcpClient(tcpClient);

            if (_commandExecutor.InProcessIpList.Contains(ip))
            {
                return;
            }

            var commandString = NetworkHelper.GetStringFromNetworkStream(tcpClient.GetStream());
            Console.WriteLine("Got command {0}", commandString);

            var command = _commandValidator.TryGetCommand(commandString);
            if (command == null)
            {
                return;
            }

            
            ExecuteCommandAndSendResult(command, commandString, ip);
        }

        public void Start()
        {
            Task.Factory.StartNew(() => _recieverTcpController.Listen());
            Task.Factory.StartNew(() => _authTcpListener.Start());
        }

        private void ExecuteCommandAndSendResult(Command command, string commandString, string ip)
        {
            var result = _commandExecutor.ExecuteCommand(command, commandString, ip);

            _senderTcpController.SendText(result, ip);
        }
    }
}
/**
 * Здесь хорошая иллюстрация того, почему нужно писать на уровне интерфейсов:
 * этот класс очень трудно будет тестировать, т.к. трудно эмулировать tcp соединение в тесте.
 * А можно было бы реализовать логику получения/отправления на уровне интерфейсов так, чтобы
 * можно было легко сделать моки.
*/