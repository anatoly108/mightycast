﻿namespace RCServer.Commands
{
    public interface IInteractiveActivationFunction
    {
        bool IsInteractiveActivated(string commandString);
    }
}