﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Sockets;
using System.Threading;
using Library;

namespace RCServer.Commands
{
    public class CommandExecutor
    {
        private readonly ITcpController _senderTcpController;
        private readonly ITcpController _recieverTcpController;

        public List<string> InProcessIpList { get; private set; } 

        public CommandExecutor(ITcpController senderTcpController, ITcpController recieverTcpController)
        {
            _senderTcpController = senderTcpController;
            _recieverTcpController = recieverTcpController;

            InProcessIpList = new List<string>();
        }

        public string ExecuteCommand(Command command, string commandString, string fromIp)
        {
            var procStartInfo = new ProcessStartInfo("cmd", "/c " + commandString)
            {
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            };

            if (command.IsInteractive)
            {
                var isInteractiveActivated = command.InteractiveActivationFunction.IsInteractiveActivated(commandString);
                if (isInteractiveActivated)
                {
                    procStartInfo.RedirectStandardInput = true;
                    var proc = new Process
                    {
                        StartInfo = procStartInfo
                    };
                    var processShell = new ProcessShell(proc, fromIp, _senderTcpController);
                    processShell.Process.Start();

                    _senderTcpController.SendText("Interactive command started", fromIp);
                    InProcessIpList.Add(fromIp);

                    processShell.Process.BeginOutputReadLine();
                    var auto = new EventWaitHandle(false, EventResetMode.AutoReset);

                    TcpController.ConnectionHandler eventHandler = delegate(TcpController sender, TcpClient tcpClient)
                    {
                        
                        var ip = sender.GetIpFromTcpClient(tcpClient);
                        if (ip != fromIp)
                        {
                            return;
                        }
                        var text = NetworkHelper.GetStringFromNetworkStream(tcpClient.GetStream());
                        processShell.Process.StandardInput.WriteLine(text);
                        auto.Set();
                    };
                    _recieverTcpController.ConnectionEvent += eventHandler;

                    auto.WaitOne();
                    Thread.Sleep(1000);
                    while (CheckIfRunning(command.Name))
                    {
                        auto.WaitOne();
                        Thread.Sleep(1000);
                    }

                    InProcessIpList.Remove(fromIp);
                    _recieverTcpController.ConnectionEvent -= eventHandler;

                    _senderTcpController.SendText("Interactive command quitted", fromIp);
                    return "";
                }
            }

            var process = new Process {StartInfo = procStartInfo};
            process.Start();

            var result = process.StandardOutput.ReadToEnd();
            return result;
        }

        private static bool CheckIfRunning(string name)
        {
            Process[] pname = Process.GetProcessesByName(name);
            if (pname.Length == 0)
                return false;
            else
                return true;
        }
    }
}
