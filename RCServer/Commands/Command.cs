﻿namespace RCServer.Commands
{
    public class Command
    {
        public string Name { get; private set; }
        public bool IsInteractive { get; private set; }
        public IInteractiveActivationFunction InteractiveActivationFunction { get; private set; }

        public Command(string name, bool isInteractive, IInteractiveActivationFunction interactiveActivationFunction)
        {
            Name = name;
            IsInteractive = isInteractive;
            InteractiveActivationFunction = interactiveActivationFunction;
        }
    }
}