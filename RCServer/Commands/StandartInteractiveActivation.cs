﻿namespace RCServer.Commands
{
    public class StandartInteractiveActivation : IInteractiveActivationFunction
    {
        public bool IsInteractiveActivated(string commandString)
        {
            if (commandString.Split(' ').Length == 1)
                return true;
            return false;
        }
    }
}