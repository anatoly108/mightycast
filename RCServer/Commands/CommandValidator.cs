﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Enumerable = System.Linq.Enumerable;

namespace RCServer.Commands
{
    public class CommandValidator
    {
        private readonly List<Command> _commands = new List<Command>
        {
            CommandFactory.Create("ipconfig"),
            CommandFactory.Create("help"),
            CommandFactory.Create("systeminfo"),
            CommandFactory.Create("ping"),
            CommandFactory.CreateStandartActivationInteractive("nslookup")
        };

        public Command TryGetCommand(string commandString)
        {
            var commandWithoutArguments = commandString.Split(' ')[0];
            var command = Enumerable.FirstOrDefault(_commands, x => x.Name == commandWithoutArguments);
            return command;
        }
    }
}