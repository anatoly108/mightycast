﻿using System.Diagnostics;
using Library;

namespace RCServer.Commands
{
    public class ProcessShell
    {
        public Process Process { get; private set; }
        private string Ip { get; set; }

        private readonly ITcpController _tcpController;

        public ProcessShell(Process process, string ip, ITcpController tcpController)
        {
            Process = process;
            Ip = ip;
            _tcpController = tcpController;
            process.OutputDataReceived += ProcessOnOutputDataReceived;
        }

        private void ProcessOnOutputDataReceived(object sender, DataReceivedEventArgs dataReceivedEventArgs)
        {
            if (dataReceivedEventArgs.Data != null)
            {
                _tcpController.SendText(dataReceivedEventArgs.Data, Ip);
            }
        }
    }
}