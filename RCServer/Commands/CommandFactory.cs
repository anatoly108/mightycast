﻿namespace RCServer.Commands
{
    public static class CommandFactory
    {
        public static Command Create(string name)
        {
            return new Command(name, false, null);
        }

        public static Command CreateStandartActivationInteractive(string name)
        {
            return new Command(name, true, new StandartInteractiveActivation());
        }
    }
}