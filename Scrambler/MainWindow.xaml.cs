﻿using System.Windows;

namespace Scrambler
{
    using System;
    using System.IO;
    using System.Net;

    using Library;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ScramblerController _scramblerController;

        private const string SettingPath = "Settings/config.ini";

        public MainWindow()
        {
            InitializeComponent();

            var iniReader = new IniFileReader(SettingPath);
            var port = int.Parse(iniReader.IniReadValue("BASIC", "port"));
            var scrambling = new Scrambling();
            var tcpController = new TcpController(port, null);

            _scramblerController = new ScramblerController(scrambling, tcpController);
            _scramblerController.GotFile += ScramblerControllerOnGotFile;
        }

        private void ScramblerControllerOnGotFile()
        {
            Dispatcher.BeginInvoke((Action)(() => WaitingTextBlock.Text = "Получен файл"));
            Dispatcher.BeginInvoke((Action)(() => DecodeAndSaveButton.IsEnabled = true));
            Dispatcher.BeginInvoke((Action)(() => SaveButton.IsEnabled = true));
        }

        private void ChooseFile(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.OpenFileDialog();
            var result = dlg.ShowDialog();
            if (result == true)
            {
                FileNameTextBox.Text = dlg.FileName;
            }
        }

        private void EncodeAndSendFile(object sender, RoutedEventArgs e)
        {
            var ip = IpMaskedTextBox.Text.Replace("_","");
            var isIpValid = IsValidIp(ip);
            if (!isIpValid)
            {
                ErrorTextBlock.Text = "Неправильный айпи";
                return;
            }
            var isValidFile = File.Exists(FileNameTextBox.Text);
            if (!isValidFile)
            {
                ErrorTextBlock.Text = "Нет такого файла";
                return;
            }

            var encoded = _scramblerController.OpenAndEncode(FileNameTextBox.Text, ScramblerTextBox.Text, KeyTextBox.Text);
            var succed = _scramblerController.SendEncodedFileBytes(encoded, ip);
            if (!succed)
            {
                ErrorTextBlock.Text = "Не удалось отправить файл";
            }
        }
        
        private void DecodeAndSaveFile(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.SaveFileDialog();
            var result = dlg.ShowDialog();
            if (result != true)
            {
                return;
            }
            var decoded = _scramblerController.Decode(ScramblerTextBox.Text, KeyTextBox.Text);
            _scramblerController.Save(dlg.FileName, decoded);
            DecodeAndSaveButton.IsEnabled = false;
            WaitingTextBlock.Text = "Ожидание файла от другого скремблера...";
        }

        private void SaveEncodedFile(object sender, RoutedEventArgs e)
        {
            var dlg = new Microsoft.Win32.SaveFileDialog();
            var result = dlg.ShowDialog();
            if (result != true)
            {
                return;
            }
            _scramblerController.Save(dlg.FileName, _scramblerController.AcceptedFileBytes);
            DecodeAndSaveButton.IsEnabled = false;
            SaveButton.IsEnabled = false;
            WaitingTextBlock.Text = "Ожидание файла от другого скремблера...";
        }

        private bool IsValidIp(string addr)
        {
            IPAddress ip;
            var valid = !string.IsNullOrEmpty(addr) && IPAddress.TryParse(addr, out ip);
            return valid;
        }
    }
}
