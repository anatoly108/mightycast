﻿namespace Scrambler
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net.Sockets;
    using System.Threading.Tasks;

    using Library;

    public class ScramblerController
    {
        public Scrambling Scrambling { get; private set; }

        private readonly TcpController _tcpController;

        public delegate void GotFileHandler();

        public event GotFileHandler GotFile;

        public byte[] AcceptedFileBytes { get; private set; }

        public ScramblerController(Scrambling scrambling, TcpController tcpController)
        {
            Scrambling = scrambling;
            _tcpController = tcpController;

            _tcpController.ConnectionEvent += TcpControllerOnConnectionEvent;

            Task.Factory.StartNew(() => _tcpController.Listen());
        }

        private void TcpControllerOnConnectionEvent(TcpController sender, TcpClient tcpClient)
        {
            AcceptedFileBytes = NetworkHelper.ReadFromNetworkStream(tcpClient.GetStream());
            GotFile();
        }

        public byte[] OpenAndEncode(string filePath, string scrambler, string key)
        {
            var fileBytes = File.ReadAllBytes(filePath);
            return Scrambling.Handle(scrambler, key, fileBytes).ToArray();
        }

        public byte[] Decode(string scrambler, string key)
        {
            return Scrambling.Handle(scrambler, key, AcceptedFileBytes).ToArray();
        }

        public void Save(string pathToSave, byte[] fileBytes)
        {
            File.WriteAllBytes(pathToSave, fileBytes);
        }

        public bool SendEncodedFileBytes(byte[] encodedBytes, string ip)
        {
            try
            {
                _tcpController.SendData(encodedBytes, ip);
                return true;
            }
            catch (Exception exception)
            {
                return false;
            }
        }
    }
}