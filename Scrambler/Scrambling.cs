﻿using System;

namespace Scrambler
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public class Scrambling
    {
        public IEnumerable<byte> Handle(string scrambler, string key, byte[] fileBytes)
        {
            var scrambledBitsBool = new List<bool>();
            var scramblerArray = scrambler.ToCharArray().Select(x => byte.Parse(x.ToString()));
            var keyArray = key.ToCharArray().Select(x => byte.Parse(x.ToString()));

            var bits = new BitArray(fileBytes);

            while (scrambledBitsBool.Count < bits.Length)
            {
                var xorResult = XorKey(scramblerArray, keyArray);
                var pushedNumber = keyArray.Last();
                keyArray = ShiftKey(keyArray.ToArray(), xorResult);

                scrambledBitsBool.Add(pushedNumber == 1);
            }

            var scrambledBits = new BitArray(scrambledBitsBool.ToArray());

            var resultBits = XorBits(bits, scrambledBits);

            var resultBytes = BitToBytes(resultBits);

            return resultBytes;
        }

        public byte[] BitToBytes(bool[] bits)
        {
            var bytesLength = bits.Length / 8;
            if ((bits.Length % 8) != 0)
            {
                bytesLength++;
            }
            var result = new byte[bytesLength];
            var bitIndex = 0;
            var byteIndex = 0;
            for (var i = 0; i < bits.Length; i++)
            {
                if (bits[i])
                {
                    result[byteIndex] |= (byte)(1 << bitIndex);
                }
                bitIndex++;
                if (bitIndex != 8)
                {
                    continue;
                }
                bitIndex = 0;
                byteIndex++;
            }

            return result;
        }

        private byte[] ShiftKey(byte[] keyArray, byte xorResult)
        {
            var result = new byte[keyArray.Length];

            for (int i = 0; i < result.Length - 1; i++)
            {
                result[i + 1] = keyArray[i];
            }
            result[0] = xorResult;
            return result;
        }

        public byte XorKey(IEnumerable<byte> scramblerArray, IEnumerable<byte> keyArray)
        {
            var indexes =
                scramblerArray.Select((number, index) => new { number, index })
                    .Where(x => x.number == 1)
                    .Select(x => x.index);

            var keyValues =
                keyArray.Select((number, index) => new { number, index })
                    .Where(x => indexes.Contains(x.index))
                    .Select(x => x.number)
                    .ToArray();

            var xorResult = keyValues[0];
            foreach (var keyValue in keyValues.Skip(1))
            {
                xorResult = (byte)(xorResult ^ keyValue);
            }

            return xorResult;
        }

        private bool[] XorBits(BitArray first, BitArray second)
        {
            if (first.Length != second.Length)
            {
                throw new Exception("Массивы разной длины");
            }

            var result = new bool[first.Length];

            for (int i = 0; i < result.Length; i++)
            {
                var xorResult = first.Get(i) ^ second.Get(i);
                result[i] = xorResult;
            }

            return result;
        }
    }
}