﻿namespace Scrambler
{
    using System.ComponentModel;

    public class ScramblerViewModel
    {
        private ScramblerController _scramblerController;

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}