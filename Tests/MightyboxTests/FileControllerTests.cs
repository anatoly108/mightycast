﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mightybox;
using Rhino.Mocks;

namespace Tests.MightyboxTests
{
    [TestClass]
    public class FileControllerTests
    {
        [TestMethod]
        public void DoubleQueryTest()
        {
            var fileQuerySender = MockRepository.GenerateStrictMock<IFileQuerySender>();
            fileQuerySender.Expect(x => x.Send(null, null)).IgnoreArguments().Repeat.Once();

            var target = new FileController(null, null, fileQuerySender, null);

            var fileName = "fileName";
            var timeForFile = new DateTime(2014, 5, 17);
            target.SendQuery("ip", fileName, timeForFile);
            target.SendQuery("ip", fileName, timeForFile);
        }
    }
}