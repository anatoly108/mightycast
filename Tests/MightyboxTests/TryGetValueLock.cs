﻿namespace Tests.MightyboxTests
{
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using System.Threading.Tasks;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using NLog.Targets;

    [TestClass]
    public class TryGetValueLock
    {
        [TestMethod]
        public void Test()
        {
            var target = new Dictionary<string, string>() { { "11", "1!" } };
            Task.Factory.StartNew(
                () =>
                {
                    lock (target)
                    {
                        Console.WriteLine("Im only sleeping");
                        Thread.Sleep(10000);
                        Console.WriteLine("Awakened");
                    }
                });
            Task.Factory.StartNew(
                () =>
                    {
                        string value;
                        lock (target)
                            target.Add("aa","aa");
                        Console.WriteLine("ok!");
                        //if (target.TryGetValue("11", out value)) Console.WriteLine("ok!");
                    });
        }
    }
}