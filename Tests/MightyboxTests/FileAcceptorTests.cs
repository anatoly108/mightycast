﻿namespace Tests.MightyboxTests
{
    using System.IO;

    using Library;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Mightybox;

    using NLog;

    [TestClass]
    public class FileAcceptorTests
    {
        [TestMethod]
        public void OneFileTest()
        {
            Helper.CreateEmptyTestDirectory();

            const string FolderName = "new_folder\\folder";
            var folderPath = Path.Combine(Constants.TestSyncFolder, FolderName);
            const string NewFileName = "newFile.txt";
            var newFilePath = Path.Combine(folderPath, NewFileName);

            Directory.CreateDirectory(folderPath);
            var testFilePath = Path.Combine(Constants.TestSyncFolder, "file.txt");
            var fileStream = File.Create(testFilePath);
            fileStream.Close();
            File.WriteAllText(testFilePath, @"content");

            var logger = LogManager.GetLogger("ConsoleLog");
            var target = new FileAcceptor(logger);
            var fileStreamWriter = new FileStreamWriter();
            var stream = new MemoryStream();
            fileStreamWriter.Write(stream, testFilePath, newFilePath);
            stream.Seek(0, SeekOrigin.Begin);
            var fileName = target.GetFileNameFromStream(stream);
            target.Accept(stream, fileName);
        }

        [TestMethod]
        public void EmptyFolderTest()
        {
            Helper.CreateEmptyTestDirectory();

            var logger = LogManager.GetLogger("ConsoleLog");
            var target = new FileAcceptor(logger);
            var fileStreamWriter = new FileStreamWriter();
            var stream = new MemoryStream();

            var testFolderPath = Path.Combine(Constants.TestSyncFolder, "empty_folder");
            var newPath = Path.Combine(Constants.TestSyncFolder, "empty_new");

            Directory.CreateDirectory(testFolderPath);

            fileStreamWriter.Write(stream, testFolderPath, newPath);
            stream.Seek(0, SeekOrigin.Begin);

            target.Accept(stream, null);
        }

        [TestMethod]
        public void EmptyFileTest()
        {
            Helper.CreateEmptyTestDirectory();

            var logger = LogManager.GetLogger("ConsoleLog");
            var target = new FileAcceptor(logger);
            var fileStreamWriter = new FileStreamWriter();
            var stream = new MemoryStream();

            const string FileName = @"file.txt";
            const string NewFileName = @"fileAccepted.txt";
            var filePath = Path.Combine(Constants.TestSyncFolder, FileName);
            var fileStream = File.Create(filePath);
            fileStream.Close();

            var pathToEncode = Path.Combine(Constants.TestSyncFolder, NewFileName);
            fileStreamWriter.Write(stream, filePath, pathToEncode);
            stream.Seek(0, SeekOrigin.Begin);
            target.Accept(stream, null);

            Assert.IsTrue(File.Exists(pathToEncode));
        }
    }
}
