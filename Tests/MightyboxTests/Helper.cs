﻿namespace Tests.MightyboxTests
{
    using System.IO;

    public static class Helper
    {
        public static void CreateEmptyTestDirectory()
        {
            var directory = new DirectoryInfo(Constants.TestSyncFolder);
            if (Directory.Exists(Constants.TestSyncFolder))
            {
                directory.Delete(true);
            }
            Directory.CreateDirectory(Constants.TestSyncFolder);
        }
    }
}