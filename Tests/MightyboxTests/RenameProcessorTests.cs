﻿namespace Tests.MightyboxTests
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Mightybox;
    using Mightybox.Processors;

    [TestClass]
    public class RenameProcessorTests
    {
        // TODO: Эти тесты работают с одной директорией и поэтому мешают друг другу
        /**
         * Можно сделать так, чтобы каждый тест создавал свою директорию, а в конце удалял её
         */

        [TestMethod]
        public void RenamingEmptyDirectoryTest()
        {
            Helper.CreateEmptyTestDirectory();

            var pathToOurDirectory = Path.Combine(Constants.TestSyncFolder, "new_empty");
            var pathToTheirDirectory = Path.Combine(Constants.TestSyncFolder, "new_empty_renamed");
            Directory.CreateDirectory(pathToOurDirectory);

            var target = new RenameProcessor();

            var directoryInfo = new FileInfoSerializable(pathToOurDirectory, DateTime.Now);
            var fileInfoSerializables = new List<FileInfoSerializable> { directoryInfo };
            var our = new FileInfoCollectionSerializable(fileInfoSerializables);

            var theirDirecoryInfo = new FileInfoSerializable(pathToTheirDirectory, DateTime.Now);
            var theirFileInfoSerializables = new List<FileInfoSerializable> { theirDirecoryInfo };
            var their = new FileInfoCollectionSerializable(theirFileInfoSerializables);

            target.Handle(our, their);
            Assert.IsTrue(Directory.Exists(pathToTheirDirectory));
            Assert.IsFalse(Directory.Exists(pathToOurDirectory));
        }

        [TestMethod]
        public void RenamingDirectoryWithContentTest()
        {
            Helper.CreateEmptyTestDirectory();

            var pathToOurDirectory = Path.Combine(Constants.TestSyncFolder, "new_empty");
            Directory.CreateDirectory(pathToOurDirectory);
            const string FileName = "file.txt";
            var filePath = Path.Combine(pathToOurDirectory, FileName);
            var fileStream = File.Create(filePath);
            fileStream.Close();
            File.WriteAllText(filePath, @"content");

            var pathToTheirDirectory = Path.Combine(Constants.TestSyncFolder, "new_empty_renamed");
            var theirFile = Path.Combine(pathToTheirDirectory, FileName);

            var directoryInfo = new FileInfoSerializable(pathToOurDirectory, DateTime.Now);
            var fileInfo = new FileInfoSerializable(filePath, DateTime.Now);
            var fileInfoSerializables = new List<FileInfoSerializable> { directoryInfo, fileInfo};
            var our = new FileInfoCollectionSerializable(fileInfoSerializables);

            var theirDirecoryInfo = new FileInfoSerializable(pathToTheirDirectory, DateTime.Now);
            var theirFileInfo = new FileInfoSerializable(theirFile, DateTime.Now);
            var theirFileInfoSerializables = new List<FileInfoSerializable> { theirDirecoryInfo, theirFileInfo };
            var their = new FileInfoCollectionSerializable(theirFileInfoSerializables);

            var target = new RenameProcessor();
            target.Handle(our, their);

            Assert.IsTrue(Directory.Exists(pathToTheirDirectory));
            Assert.IsFalse(Directory.Exists(pathToOurDirectory));
        }
    }
}