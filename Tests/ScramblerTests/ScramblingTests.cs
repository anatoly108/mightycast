﻿namespace Tests.ScramblerTests
{
    using System.Linq;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Scrambler;

    [TestClass]
    public class ScramblingTests
    {
        [TestMethod]
        public void Test1()
        {
            var target = new Scrambling();
            var scrambler = "001100";
            var key = "000100";
            var fileBytes = new byte[] { 1, 2, 4, 2, 1 };
            var encoded = target.Handle(scrambler, key, fileBytes);
            var decoded = target.Handle(scrambler, key, encoded.ToArray());
            CollectionAssert.AreEqual(fileBytes, decoded.ToArray());
        }
    }
}