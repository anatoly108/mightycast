﻿using System;
using System.Net.Sockets;
using Library;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RCServer;
using RCServer.Commands;
using Rhino.Mocks;

namespace Tests.RCTests
{
    [TestClass]
    public class CommandExecutorTests
    {
        delegate void WriteLine(string text);

        private void Write(string text)
        {
            Console.WriteLine(text);
        }

//        [TestMethod]
//        public void NslookupInteractiveExitTest()
//        {
//            var fromIp = "";
//            var command = CommandFactory.CreateStandartActivationInteractive("nslookup");
//            var commandString = "nslookup";
//
//            var tcpController = MockRepository.GenerateStrictMock<ITcpController>();
//            tcpController.Expect(x => x.SendText(null, null)).IgnoreArguments().Repeat.Any();
//            tcpController.Expect(x => x.GetIpFromTcpClient(null)).IgnoreArguments().Return(fromIp).Repeat.Any();
//            tcpController.Expect(x => x.GetStringFromNetworkStream(null)).IgnoreArguments().Return("exit").Repeat.Any();
//
//            var tcpListener = MockRepository.GenerateStrictMock<ITcpListener>();
//            tcpListener.Expect(x => x.AcceptTcpClient()).Return(new TcpClient()).Repeat.Any();
//
//            var target = new CommandExecutor(tcpController, tcpListener);
//            
//            target.ExecuteCommand(command, commandString, fromIp);
//        }

        [TestMethod]
        public void SimpleCommandTest()
        {
            var target = new CommandExecutor(null, null);
            var fromIp = "";
            var commandString = "ipconfig";
            var command = CommandFactory.Create("ipconfig");

            target.ExecuteCommand(command, commandString, fromIp);
        }
    }
}