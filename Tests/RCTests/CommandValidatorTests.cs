﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RCServer;
using RCServer.Commands;

namespace Tests.RCTests
{
    [TestClass]
    public class CommandValidatorTests
    {
        [TestMethod]
        public void IpConfigTest()
        {
            var target = new CommandValidator();
            var actual = target.TryGetCommand("ipconfig");

            Assert.IsTrue(actual != null);
        }

        [TestMethod]
        public void FormatTest()
        {
            var target = new CommandValidator();
            var actual = target.TryGetCommand("format c");

            Assert.IsTrue(actual == null);
        }
    }
}