﻿using System;
using System.Net.Sockets;
using System.Threading.Tasks;
using Library;

namespace RCClient
{
    public class ClientController
    {
        private readonly TcpController _senderTcpController;
        private readonly TcpController _recieverTcpController;

        public ClientController(TcpController senderTcpController, TcpController recieverTcpController)
        {
            _senderTcpController = senderTcpController;
            _recieverTcpController = recieverTcpController;

            _recieverTcpController.ConnectionEvent += RecieverTcpControllerOnConnectionEvent;
        }

        private void RecieverTcpControllerOnConnectionEvent(TcpController sender, TcpClient tcpClient)
        {
            var result = NetworkHelper.GetStringFromNetworkStream(tcpClient.GetStream());
            Console.WriteLine(result);
        }

        public void Start()
        {
            Task.Factory.StartNew(() => _recieverTcpController.Listen());
        }

        public void SendCommand(string command, string ip)
        {
            _senderTcpController.SendText(command, ip);
        }
    }
}