﻿using System;

using Library;

namespace RCClient
{
    using System.Threading;

    internal class Program
    {
        private const string ConfigPath = "Settings\\config.ini";

        private static bool _isAuth;
        private static readonly EventWaitHandle AuthEventWaitHandle = new EventWaitHandle(false, EventResetMode.AutoReset);

        private static void Main(string[] args)
        {
            var iniReader = new IniFileReader(ConfigPath);
            var iniReadValue = iniReader.IniReadValue("BASIC", "serverPort");
            var serverPort = int.Parse(iniReadValue);
            var clientPort = int.Parse(iniReader.IniReadValue("BASIC", "clientPort"));
            var authPort = int.Parse(iniReader.IniReadValue("BASIC", "authPort"));
            var serverIp = iniReader.IniReadValue("BASIC", "serverIp");

            var authenticatingTcpClient = new AuthenticatingTcpClient(serverIp, authPort);
            authenticatingTcpClient.AuthenticationEndEvent += AuthenticatingTcpClientOnAuthenticationEndEvent;

            var senderTcpController = new TcpController(clientPort, null);
            var recieverTcpController = new TcpController(serverPort, null);

            try
            {
                senderTcpController.SendText("", serverIp);
            }
            catch (Exception exception)
            {
                Console.WriteLine("Сервер консоли недоступен");
                Console.ReadKey();
                return;
            }

            Console.Write("Username: ");
            var username = Console.ReadLine();
            Console.Write("Password: ");
            var password = Console.ReadLine();

            authenticatingTcpClient.Authenticate(username, password);

            Console.WriteLine("Waiting for response...");
            AuthEventWaitHandle.WaitOne();

            if (!_isAuth)
            {
                Console.WriteLine("Wrong data");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("Ok");
            var controller = new ClientController(senderTcpController, recieverTcpController);
            controller.Start();

            Console.WriteLine("Type EXIT to quit");

            var input = Console.ReadLine();

            while (input != "EXIT")
            {
                controller.SendCommand(input, serverIp);
                input = Console.ReadLine();
            }
        }

        private static void AuthenticatingTcpClientOnAuthenticationEndEvent(object sender, bool result)
        {
            _isAuth = result;
            AuthEventWaitHandle.Set();
        }
    }
}