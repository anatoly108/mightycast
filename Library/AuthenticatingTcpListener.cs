﻿using System;
using System.Text;

namespace Library
{
    using System.Net;
    using System.Net.Security;
    using System.Net.Sockets;
    using System.Security.Authentication;
    using System.Threading;

    public class AuthenticatingTcpListener
    {
        private readonly string _ip;
        private readonly int _port;

        public AuthenticatingTcpListener(string ip, int port)
        {
            _ip = ip;
            _port = port;
        }

        public void Start()
        {
            var listener = new TcpListener(IPAddress.Parse(_ip), _port);
            listener.Start();
            while (true)
            {
                var clientRequest = listener.AcceptTcpClient();
                Console.WriteLine("Client connected.");
                try
                {
                    AuthenticateClient(clientRequest);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private static void AuthenticateClient(TcpClient clientRequest)
        {
            var stream = clientRequest.GetStream();
            var authStream = new NegotiateStream(stream, false);
            var cState = new ClientState(authStream, clientRequest);
            authStream.BeginAuthenticateAsServer(
                EndAuthenticateCallback,
                cState
                );
            cState.Waiter.WaitOne();
            cState.Waiter.Reset();
            authStream.BeginRead(cState.Buffer, 0, cState.Buffer.Length,
                   EndReadCallback,
                   cState);
            cState.Waiter.WaitOne();
            authStream.Close();
            clientRequest.Close();
        }

        private static void EndAuthenticateCallback(IAsyncResult ar)
        {
            var cState = (ClientState)ar.AsyncState;
            var authStream = (NegotiateStream)cState.AuthenticatedStream;
            Console.WriteLine("Ending authentication.");
            try
            {
                authStream.EndAuthenticateAsServer(ar);
            }
            catch (AuthenticationException e)
            {
                Console.WriteLine(e);
                Console.WriteLine("Authentication failed - closing connection.");
                cState.Waiter.Set();
                return;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine("Closing connection.");
                cState.Waiter.Set();
                return;
            }
            var id = authStream.RemoteIdentity;
            Console.WriteLine("{0} was authenticated using {1}.",
                id.Name,
                id.AuthenticationType
                );
            cState.Waiter.Set();
        }

        private static void EndReadCallback(IAsyncResult ar)
        {
            var cState = (ClientState)ar.AsyncState;
            var authStream = (NegotiateStream)cState.AuthenticatedStream;
            var bytes = -1;
            try
            {
                bytes = authStream.EndRead(ar);
                cState.Message.Append(Encoding.UTF8.GetChars(cState.Buffer, 0, bytes));
                if (bytes != 0)
                {
                    authStream.BeginRead(cState.Buffer, 0, cState.Buffer.Length,
                          EndReadCallback,
                          cState);
                    return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Client message exception:");
                Console.WriteLine(e);
                cState.Waiter.Set();
                return;
            }
            var id = authStream.RemoteIdentity;
            Console.WriteLine("{0} says {1}", id.Name, cState.Message.ToString());
            cState.Waiter.Set();
        }
    }
    internal class ClientState
    {
        private readonly AuthenticatedStream authStream = null;
        private readonly TcpClient client = null;

        readonly byte[] _buffer = new byte[2048];
        StringBuilder _message = null;

        readonly ManualResetEvent waiter = new ManualResetEvent(false);
        internal ClientState(AuthenticatedStream a, TcpClient theClient)
        {
            authStream = a;
            client = theClient;
        }
        internal TcpClient Client
        {
            get { return client; }
        }
        internal AuthenticatedStream AuthenticatedStream
        {
            get { return authStream; }
        }
        internal byte[] Buffer
        {
            get { return _buffer; }
        }
        internal StringBuilder Message
        {
            get
            {
                if (_message == null)
                    _message = new StringBuilder();
                return _message;
            }
        }
        internal ManualResetEvent Waiter
        {
            get
            {
                return waiter;
            }
        }
    }
}
