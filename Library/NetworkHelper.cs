﻿namespace Library
{
    using System.IO;
    using System.Net.Sockets;

    public static class NetworkHelper
    {
        public static byte[] ReadFromNetworkStream(NetworkStream networkStream)
        {
            var recData = new byte[Constants.NetworkReaderBufferSize];
            var memoryStream = new MemoryStream();
            int recBytes;
            while ((recBytes = networkStream.Read(recData, 0, recData.Length)) > 0)
            {
                memoryStream.Write(recData, 0, recBytes);
            }
            memoryStream.Seek(0, SeekOrigin.Begin);

            var result = new byte[memoryStream.Length];
            memoryStream.Read(result, 0, (int)(memoryStream.Length));

            return result;
        }

        public static string GetStringFromNetworkStream(NetworkStream networkStream)
        {
            var reader = new StreamReader(networkStream, Constants.Encoding);
            var message = reader.ReadToEnd();
            return message;
        }
    }
}