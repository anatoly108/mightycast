﻿using System;

namespace Library
{
    using System.Net;
    using System.Net.Security;
    using System.Net.Sockets;

    public class AuthenticatingTcpClient
    {
        private TcpClient _client;

        private readonly string _serverIp;

        private readonly int _port;

        public delegate void AuthEvent(object sender, bool result);

        public event AuthEvent AuthenticationEndEvent;

        public AuthenticatingTcpClient(string serverIp, int port)
        {
            _serverIp = serverIp;
            _port = port;
        }

        public void Authenticate(string userName, string password)
        {
            var ipAddress = IPAddress.Parse(_serverIp);
            var remoteEp = new IPEndPoint(ipAddress, _port);
            _client = new TcpClient();
            _client.Connect(remoteEp);
            _client.LingerState = (new LingerOption(true, 0));

            var clientStream = _client.GetStream();
            var authStream = new NegotiateStream(clientStream, false);

            var networkCredential = new NetworkCredential(userName, password);
            var ar = authStream.BeginAuthenticateAsClient(networkCredential, "", EndAuthenticateCallback, authStream);

            ar.AsyncWaitHandle.WaitOne();
            AuthenticationEndEvent(this, authStream.IsAuthenticated);
            authStream.Close();
        }

        private static void EndAuthenticateCallback(IAsyncResult ar)
        {
            var authStream = (NegotiateStream)ar.AsyncState;
            try
            {
                authStream.EndAuthenticateAsClient(ar);
            }
            catch (Exception exception)
            {
                Console.WriteLine("Auth failed");
            }
        }
    }
}