﻿using System.Net;
using System.Threading.Tasks;

namespace Library
{
    using System;
    using System.Net.Sockets;

    using NLog;

    public class TcpController : ITcpController
    {
        public int Port { get; private set; }

        private readonly TcpListener _tcpListener;

        private readonly Logger _logger;

        public delegate void ConnectionHandler(TcpController sender, TcpClient tcpClient);

        public event ConnectionHandler ConnectionEvent;

        public TcpController(int port, Logger logger)
        {
            Port = port;
            _logger = logger;
            _tcpListener = new TcpListener(Port);
        }

        public void Listen()
        {
            _tcpListener.Start();
            while (true)
            {
                TcpClient client;
                try
                {
                    client = _tcpListener.AcceptTcpClient();
                }
                catch (Exception exception)
                {
                    _logger.Error(exception.ToString());
                    return;
                }

                Task.Factory.StartNew(() =>
                {
                    ConnectionEvent(this, client);
                    client.Close();
                });
            }
        }

        public void StopListen()
        {
            _tcpListener.Server.Close();
        }

        public void SendText(string text, string ip)
        {
            var tcpClient = new TcpClient(ip, Port);
            var stream = tcpClient.GetStream();

            var sendbuf = Constants.Encoding.GetBytes(text);
            stream.Write(sendbuf, 0, sendbuf.Length);
            
            tcpClient.Close();
        }

        public void SendData(byte[] data, string ip)
        {
            var tcpClient = new TcpClient(ip, Port);
            var stream = tcpClient.GetStream();

            stream.Write(data, 0, data.Length);

            tcpClient.Close();
        }

        public string GetIpFromTcpClient(TcpClient tcpClient)
        {
            return ((IPEndPoint)tcpClient.Client.RemoteEndPoint).Address.ToString();
        }
    }
}
