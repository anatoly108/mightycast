﻿using System.Net.Sockets;

namespace Library
{
    public interface ITcpListener
    {
        int Port { get; set; }
        TcpClient AcceptTcpClient();
        void Start();
    }
}