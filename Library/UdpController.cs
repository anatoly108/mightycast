﻿using System.IO;
using NLog;

namespace Library
{
    using System;
    using System.Net;
    using System.Net.Sockets;

    public class UdpController
    {
        private int Port { get; set; }
        private readonly string _multicastIp;

        private readonly Logger _logger;

        private UdpClient _udpClient;

        public delegate void ConnectionHandler(UdpController sender, string ip, MemoryStream stream);

        public event ConnectionHandler ConnectionEvent;

        public UdpController(int port, string multicastIp, Logger logger)
        {
            Port = port;
            _multicastIp = multicastIp;
            _logger = logger;
        }

        public void Listen()
        {
            _udpClient = new UdpClient(Port);
            var groupEp = new IPEndPoint(IPAddress.Any, this.Port);
            try
            {
                while (true)
                {
                    var bytes = _udpClient.Receive(ref groupEp);

                    if (bytes == null || bytes.Length == 0) return;

                    var address = groupEp.ToString().Split(':')[0];
                    if (address == GetLocalIpAddress())
                    {
                        continue;
                    }
                    var stream = new MemoryStream(bytes);
                    _logger.Info("UdpController: got connection " + address);
                    ConnectionEvent(this, address, stream);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
            }
            finally
            {
                _udpClient.Close();
            }
        }

        public void StopListen()
        {
            Send(GetLocalIpAddress(), "");
        }

        public void SendMulticast(string message)
        {
            var s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            var broadcast = IPAddress.Parse(_multicastIp);
            var ep = new IPEndPoint(broadcast, Port);
            var sendbuf = Constants.Encoding.GetBytes(message);

            try
            {
                s.SendTo(sendbuf, ep);
            }
            finally
            {
                s.Close();
                s.Dispose();
            }

            _logger.Info(string.Format("Message '{0}' sent to the broadcast address", message));
        }

        public void Send(string ip, string message)
        {
            var s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            var ipAddress = IPAddress.Parse(ip);
            var ep = new IPEndPoint(ipAddress, Port);
            var sendbuf = Constants.Encoding.GetBytes(message);

            try
            {
                s.SendTo(sendbuf, ep);
            }
            finally
            {
                s.Close();
                s.Dispose();
            }

            _logger.Info("Message sent to the address " + ip);
        }

        public void SendMulticastData(byte[] data)
        {
            var s = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            var ipAddress = IPAddress.Parse(_multicastIp);
            var ep = new IPEndPoint(ipAddress, Port);

            try
            {
                s.SendTo(data, ep);
            }
            catch (Exception exception)
            {
                _logger.Error(exception.ToString());
            }
            finally
            {
                s.Close();
                s.Dispose();
            }
        }

        public static string GetLocalIpAddress()
        {
            // TODO: Не могу всегда узнать свой адрес: их несколько, хз как узнать, какой предпочтителен
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            return "";
        }
    }
}