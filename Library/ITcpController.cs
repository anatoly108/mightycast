﻿using System.Net.Sockets;

namespace Library
{
    public interface ITcpController
    {
        event TcpController.ConnectionHandler ConnectionEvent;
        int Port { get; }
        void Listen();
        void StopListen();
        void SendText(string text, string ip);
        string GetIpFromTcpClient(TcpClient tcpClient);
    }
}