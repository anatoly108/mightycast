﻿namespace Library
{
    using System.Text;

    public static class Constants
    {
        public const int NetworkReaderBufferSize = 1024;
        public static readonly Encoding Encoding = Encoding.Default;
    }
}